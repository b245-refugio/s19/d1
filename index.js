console.log("hello");

// What is conditional statements?
	// Conditional Statements allow us to control the flow of our program.
	// It allows us to run statement/instruction if a condition is met or run another separate instruction if not otherwise.

//  [SECTION] if, else if and else statement
	let numA = -1;

/*
	if statement
		- it will the statement/code blocks if a specified condition is met/true.

*/

	if(numA<0){
		console.log("Hello from numA.");
	}

	/*
		Syntax:
		if(condition){
			statement;
		}

	*/
		// The result of expression added in the if's condition must result to true, else the statement inside if() will not run.

// lets reassign the variable numA and run an if statement with the same condition.

numA = 1;

if (numA<0) {
	console.log("Hello from the reassign value of numA.");
}
console.log(numA<0);

// it will not run because the expression now reults to false.

let city = "New York";

if (city === "New York") {
	console.log("Welcome to New York!");
}

// else if clause

/*
	-Executes a statement if previous condition are false and if the specified condition is true
	-The 'else if' is optional and can be added to capture additional condtions to change the flow of a program.
*/

let numH = 1;

if (numH<0) {
	console.log("Hello from (num<0).");

} else if(numH>0){
	console.log("Hello from (numH>0).");
}

/*
	We are able to run the else if() statement after we evaluated that the if condition was failed/false. 
*/

// if the if() condition was passed and run,we will no longor evaluate to else if () and end the process.

// else if is dependent with if, you cannot use else if clause alone.
/*{
	else if (numH>0){
	console.log("Hello from (numH>0).");	
	}
}*/

if (numH!==1){
	console.log("hello from numH === 1!");
} else if(numH<0){
	console.log("hello form numH>0!");
} else if(numH>0){
	console.log("Hello from the second else if clause!");
}

city = "Tokyo";

if (city === "New York") {
	console.log("Welcome to New York!");
} else if(city === "Tokyo"){
	console.log("Welcome to Tokyo!");
}

// else statement
	/*
		- Executes a stement if all other conditions are false/ not met
		- else if statement is optional and can be added to capture any other result to change the flow of program.
	*/

numH = 2;

if (numH<0) {
	console.log("Hello from if statement");
} else if(numH>2){
	console.log("Hello from the first else if");
} else if (numH>3){
	console.log("Hello from the second else if");
} else {
	console.log("Hello from the else statement");
}

// Since all of the preceeding if and else conditions we not met, the else staement was execited instead.
// Else stament is also dependent with if statement, it cannot go alone.

/*{
	else{
		console.log("Hellow from the else inside the code block");
	}
}*/

 	// if, else if and else statement with functions
 	/*
		Most of the times we would like to use if, else if and if statement with functions to control the flow of out application.
	
 	*/ 

let message;

function determineTyphoonIntensity(windSpeed){
	if (windSpeed<0) {
		return "invalid argument";
	} else if(windSpeed<=30){
		return "Not a typhoon yet!";
	} else if (windSpeed<=60) {
		return  "Tropical despression detected!";
	} else if(windSpeed<=88){
		return "Tropical storm Detected"
	} else if (windSpeed<=117){
		return "Severe Tropical Storm Detected"
	} else if (windSpeed>117) {
		return "Typhoon detected!"
	}
}

// mini-activity
// 1. add return Tropical storm detected if the windSpeed is between 60 and 89
// 2. add return "Severe Tropical Storm Detected" if the windspeed is between 88 and 118
// 3. if higher than 117 return "Typhoon detected!"
console.log(determineTyphoonIntensity(88));

message = determineTyphoonIntensity(119);

if (message === "Typhoon detected!") {
	// console.warn() is a good way to print warning in our console that could help us developers act on certain output within our code.
	console.warn(message);
}

// [SECTION] Truthy or falsy
	// javascript a "truthly" is a value is considered true when encountered in Boolean constext.
	// values are considered truless defined otherwise.

	// falsy/values/ exceptions for truthy
	/*
		1.false
		2.0
		3.-0
		4.""
		5.null
		6.undifined
		7.Nan - not a number
	
	*/

if (true) {
	console.log("true");
}

if (1) {
	console.log("true");
}

if (null) {
	console.log("Hello from null inside the if condition");
} else {
	console.log("Hello from the else of the null condition");
}

// [SECTION] Conditional Operator Ternary operator
	/*
		The conditional operator
		1. condition
		2. expression to execute if the condition is true  or truthy
		3. expression if the condition is falsy
		Syntax:
		(expression) ? ifTrue : ifFalse;
	*/
	
	let ternaryResult = (1>18)? 1 :2;
	console.log(ternaryResult);

// [SECTION] Switch Statement
	/*
		The switch statemenr evaluates an expression and matchess the expression's value to a cas class.
	*/

	let day = prompt("What day of the week today?").toLowerCase();

	switch (day){
		case 'monday':
			console.log("The color of the day is red!");
			// it means the code will stop here
			break;
		case 'tuesday':
			console.log("The color of the day is orange!");
			break;	
		case 'wednesday':
			console.log("The color of the day is yellow!");
			break;
		case 'thursday':
			console.log("The color of the day is green!");
			break;
		case 'friday':
			console.log("The color of the day is blue!");
			break;
		case 'saturday':
			console.log("The color  of the day is indigo!");
			break;
		case 'sunday':
			console.log("The color of the day is violet!");
			break;
		default:
			console.log("Please input a valid day!");
		}
// [SECTION] Try-catch-finally
		// Try-catch statement for error handling
		// There are intances when the application have returns an error/warning that is not necessarily an error in the context of our code.
	
	// These errors are result of an attempt of the programming language to help in creating effecient code.

	function showIntensity(windSpeed){
		try{
			//codes that will be executed or run
			alerat(determineTyphoonIntensity(windSpeed));
			alert("Intensity updates will show alert from try.");
		}
		catch(error){
			console.warn(error.message);
		}
		finally{
			// continue execution of code regarless of success and failure of code execution in the try statement
			alert("Intensity updates will show new alert from finally.");
		}
	}	

	showIntensity(119);